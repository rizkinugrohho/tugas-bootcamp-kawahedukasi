package org.iki;

import org.iki.models.Employee;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("employee")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class EmployeeResource {

    @GET
    public List<Employee> getEmployees() {
        return Employee.listAll();
    }

    @GET
    @Path("{id}")
    public Employee getEmployeeById(Long id) {
        return Employee.findById(id);
    }

    @POST
    @Transactional
    public List<Employee> addEmployee(Employee employee) {
        employee.persist();
        return Employee.listAll();
    }

    @PUT
    @Path("{id}")
    @Transactional
    public Employee updateEmployee(@PathParam("id") Long id, Employee newEmployee) {
        Employee oldEmployee = Employee.findById(id);
        oldEmployee.ename = newEmployee.ename;
        oldEmployee.eaddress = newEmployee.eaddress;
        oldEmployee.jobfield = newEmployee.jobfield;
        oldEmployee.salary = newEmployee.salary;
        return oldEmployee;
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Boolean deleteEmployee(@PathParam("id") Long id) {
        return Employee.deleteById(id);
    }

    @PUT
    @Path("thr/{id}")
    @Transactional
    public Employee thr(@PathParam("id") Long id, Integer newSalary) {
        Employee.update("salary = ?1 where id = ?2", newSalary, id);
        return Employee.findById(id);
    }
}