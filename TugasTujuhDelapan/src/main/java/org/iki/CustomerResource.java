package org.iki;

import org.iki.models.Customer;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("customer")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CustomerResource {

    @GET
    public List<Customer> getCustomers() {
        return Customer.listAll();
    }

    @GET
    @Path("{id}")
    public Customer getCustomerById(Long id) {
        return Customer.findById(id);
    }

    @POST
    @Transactional
    public List<Customer> addCustomer(Customer customer) {
        customer.persist();
        return Customer.listAll();
    }

    @PUT
    @Path("{id}")
    @Transactional
    public Customer updateCustomer(@PathParam("id") Long id, Customer newCustomer) {
        Customer oldCustomer = Customer.findById(id);
        oldCustomer.cname = newCustomer.cname;
        oldCustomer.caddress = newCustomer.caddress;
        oldCustomer.cstatus = newCustomer.cstatus;
        oldCustomer.cnumber = newCustomer.cnumber;
        return oldCustomer;
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Boolean deleteCustomer(@PathParam("id") Long id) {
        return Customer.deleteById(id);
    }

    @PUT
    @Path("membership/{id}")
    @Transactional
    public Customer membership(@PathParam("id") Long id, String newStatus) {
        Customer.update("cstatus = ?1 where id = ?2", newStatus, id);
        return Customer.findById(id);
    }
}