package org.iki.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;

@Entity
public class Customer extends PanacheEntity {
    public String cname;
    public String caddress;
    public String cstatus;
    public Integer cnumber;

    @JsonGetter
    public Long getId() {
        return this.id;
    }

    @JsonSetter
    @JsonIgnore
    public void setId(Long id) {
        this.id = id;
    }
}
