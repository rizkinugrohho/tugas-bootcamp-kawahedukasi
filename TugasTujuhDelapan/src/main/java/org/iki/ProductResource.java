package org.iki;

import org.iki.models.Product;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("product")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProductResource {

    @GET
    public List<Product> getProducts() {
        return Product.listAll();
    }

    @GET
    @Path("{id}")
    public Product getProductById(Long id) {
        return Product.findById(id);
    }

    @POST
    @Transactional
    public List<Product> addProduct(Product product) {
        product.persist();
        return Product.listAll();
    }

    @PUT
    @Path("{id}")
    @Transactional
    public Product updateProduct(@PathParam("id") Long id, Product newProduct) {
        Product oldProduct = Product.findById(id);
        oldProduct.brand = newProduct.brand;
        oldProduct.category = newProduct.category;
        oldProduct.details = newProduct.details;
        oldProduct.price = newProduct.price;
        return oldProduct;
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Boolean deleteProduct(@PathParam("id") Long id) {
        return Product.deleteById(id);
    }

    @PUT
    @Path("promo/{id}")
    @Transactional
    public Product promo(@PathParam("id") Long id, Integer newPrice) {
        Product.update("price = ?1 where id = ?2", newPrice, id);
        return Product.findById(id);
    }
}
