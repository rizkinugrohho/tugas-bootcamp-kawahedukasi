import java.util.Scanner;

public class TugasSatu {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Selamat Datang!!");
		System.out.println("[1] Identitas");
		System.out.println("[2] Kalkulator");
		System.out.println("[3] Perbandingan");
        System.out.print("Pilihan Menu : ");
        int inputanUser = scanner.nextInt();

        switch(inputanUser){
            // Menu 1 Identitas
            case 1:
                System.out.println("Identitas : ");
                System.out.println("Nama Lengkap : Rizki Nugroho");
                System.out.println("Alasan saya menjadi seorang back-end developer : Karena di era modern seperti sekarang pekerjaan Back-end Developer ini sangat menjanjikan.");
                System.out.println("Ekspektasi mengikuti bootcamp ini : Ingin menambah wawasan tentang Back-end Developer dan belajar hal-hal baru dari mentor yang tidak bisa didapat sendiri.");
                break;
				
            // Menu 2 Kalkulator
            case 2:
                System.out.println("Kalkulator : ");
                System.out.println("[1] Penjumlahan");
				System.out.println("[2] Pengurangan");
				System.out.println("[3] Perkalian");
				System.out.println("[4] Pembagian");
				System.out.println("[5] Sisa Bagi");
                System.out.print("Pilihan Menu Kalkulator : ");
				
                int inputanKalkulator = scanner.nextInt();
                System.out.println("Masukan angka yang akan di Hitung");
                System.out.print("Ketik angka pertama : ");
                int nomor1 = scanner.nextInt();
                System.out.print("Ketik angka kedua : ");
                int nomor2 = scanner.nextInt();
                int hasil; 
                switch(inputanKalkulator){
                    case 1:
                        hasil = nomor1 + nomor2;
                        System.out.println(hasil);
                        break;
                    case 2:
                        hasil = nomor1 - nomor2;
                        System.out.println(hasil);
                        break;
                    case 3:
                        hasil = nomor1 * nomor2;
                        System.out.println(hasil);
                        break;
                    case 4:
                        hasil = nomor1 / nomor2;
                        System.out.println(hasil);
                        break;
                    case 5:
                        hasil = nomor1 % nomor2;
                        System.out.println(hasil);
                        break;
                    default:
                        System.out.println("Harap masukan Pilihan Menu 1-5");
                }
                break;
				
            // Menu 3 Perbandingan
            case 3:
                System.out.println("Perbandingan : ");
                System.out.println("Masukkan angka yang akan di bandingkan");
                System.out.print("Ketik angka pertama : ");
                int angka1 = scanner.nextInt();
                System.out.print("Ketik angka kedua : ");
				
                int angka2 = scanner.nextInt();
                if(angka1 > angka2)
                    System.out.println(angka1 + " lebih besar dari " + angka2);
                else if(angka1 < angka2)
                    System.out.println(angka1 + " lebih kecil dari " + angka2);
                else if(angka1 == angka2)
                    System.out.println(angka1 + " sama dengan " + angka2);
                break;
            default:
                System.out.println("Harap masukan Pilihan Menu 1-3");
        }

        scanner.close();

    }
}
