import java.util.Scanner;

public class TugasDua {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
            // Input Menu Program
            System.out.println("Selamat Datang");
            System.out.println("[1] Method Pertama Absolute (Mutlak)");
			System.out.println("[2] Method Kedua Terbesar");
			System.out.println("[3] method Ketiga Perpangkatan");
            System.out.print("Silahkan Pilih Menu [1/2/3]: ");
            int inputanUser = scanner.nextInt();

            
            switch(inputanUser){
				// Method Nilai Absolute
                case 1:
                    System.out.println("Method Pertama Mengembalikan NIlai Absolute (Mutlak)");
                    System.out.print("Masukkan Nilai : ");
                    int nilai = scanner.nextInt();
                    int hasil = nilaiMutlak(nilai);
                    System.out.println("Hasil : " + hasil);
                    break;
				// Method Nilai Terbesar
                case 2:
                    System.out.println("Method Kedua mengembalikan nilai yang terbesar");
                    System.out.print("Masukkan Nilai Array yang diinput : ");
                    int myArray = scanner.nextInt();
                    System.out.print("Masukkan Nilai Terbesar dari inputan " + myArray + " : ");
                    int [] array = new int [10];
                    for(int i=0; i<myArray; i++)  
                    {  
                        array[i] = scanner.nextInt(); 
                    }  
                    int hasilMyArray = nilaiTerbesar(array);
                    System.out.println("Hasil : " + hasilMyArray);
                    break;
				// Method Nilai Perpangkatan
                case 3:
                    System.out.println("Method Ketiga mengembalikan perpangkatan");
                    System.out.print("Masukkan Nilai : ");
                    int bilangan = scanner.nextInt();
                    System.out.print("Masukkan Perpangkatan : ");
                    int pangkat = scanner.nextInt();
                    int bilanganPangkat = nilaiPangkat(bilangan, pangkat);
                    System.out.println("Hasil : " + bilanganPangkat);
                    break;
            }

    }

    public static int nilaiMutlak(int nilai){
        if(nilai < 0)
            return -nilai;
        else 
            return nilai;        
    }

    public static int nilaiTerbesar(int [] nilai){
        int terbesar = nilai[0];
        for(int i = 1; i < nilai.length; i++){
            if(nilai[i] > terbesar)
                terbesar = nilai[i];
        }       
        return terbesar; 
    }

    public static int nilaiPangkat(int n, int p){
        int hasilPangkat = n;
        for(int i=p; i > 1; i--){
            hasilPangkat = hasilPangkat * n;
        }
        return hasilPangkat;
    }
}
